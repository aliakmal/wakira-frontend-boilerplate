import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../../environments/environment';
import 'rxjs/add/operator/toPromise';
import { map } from "rxjs/operators";
import 'rxjs/Rx';
import { User } from '../../models/user.model';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

const httpPostOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'multipart/form-data' })
};

@Injectable()
export class AuthService {
  public user;
  public loading;
  public err;
  public url;
  public restaurantID;

  public gateway_error;

  constructor(private http:HttpClient) { 
    this.loading = false;
    this.url = environment.apiUrl;
    this.restaurantID = environment.restaurantID;
    this.gateway_error = '';
  }

  setGatewayError(str){
    this.gateway_error = str;
  }

  clearGatewayError(){
    this.gateway_error = '';
  }

  getGatewayError(){
    return this.gateway_error;
  }

  hasGatewayError(){
    return this.gateway_error == '' ? false : true;
  }

  register(user){
    let url = this.url+'/register';
    let response = this.http.post(url, {data:user}, httpPostOptions);
    return response;
  }

  login(user){
    let url = this.url+'/login';
    let response = this.http.post(url, {data:user}, httpPostOptions);
    return response;
  }

  refreshUserDetails(){
    let user_id = this.getUserId()
    let url = this.url+'/user-details?user_id='+user_id;
    this.http.get(url).subscribe(
      data=>{
        this.logUserIn(data['success']);
      },
      err => {
        console.log(err);
      },
      () => console.log('Done')
    );
  }


  getKey(){
    return 'user-auth';
  }

  logout(){
    localStorage.removeItem(this.getKey());
  }

  isLoggedIn(){
    if(!localStorage.getItem(this.getKey())){
      return false;
    }else{
      return true;
    }
  }

  getUserId(){
    let user = this.getLoggedInUser();
    return user['id'];
  }

  getLoggedInUser(){
    let data = JSON.parse(localStorage.getItem(this.getKey()));
    let user =  new User();
    user.initFromObject(data);

    return user;
  }


  getOrders(user){
      let url = this.url+'/orders?user_id='+user.id;
      let response = this.http.get(url);
      return response;
  }

  getAddresses(user){
    let url = this.url+'/addresses?user_id='+user.id;
    let response = this.http.get(url);
    return response;
  }

  logUserIn(user){
    localStorage.setItem(this.getKey(), JSON.stringify(user));
  }

  removeAddress(address_id){
    let url = this.url+'/addresses/delete';
    let response = this.http.post(url, {data:{ id:address_id}}, httpPostOptions);
    return response;
  }


}
