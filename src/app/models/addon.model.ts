import { AddonOption } from './addon_option.model'
export class Addon {
    name: string;
    id: number;
    min:number;
    max:number;

    options: Array<AddonOption>;
    constructor(addon){
        this.name = addon.name;
        this.id = addon.id;
        this.min = addon.min == null ? 0 : addon.min;
        this.max = addon.max == null ? 0 : addon.max;
        this.options = [];
        for(let i in addon.options){
            addon.options[i].option_of = this.name;
            let option = new AddonOption(addon.options[i])
            this.options.push(option);
        }
    }
}

