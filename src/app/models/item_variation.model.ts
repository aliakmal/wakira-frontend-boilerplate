export class ItemVariation {
    name: string;
    id: number;
    description: string;
    price: number;
    constructor(variation){
        this.name = variation.name;
        this.id = variation.id;
        this.description = variation.description;
        this.price = variation.price;
    }
}

