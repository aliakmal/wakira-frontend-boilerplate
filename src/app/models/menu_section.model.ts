import { Item } from './item.model'
import { ComboItem } from './combo_item.model'
export class MenuSection {
    id: number;
    name: string;
    menu_id: number;
    created_at: string;
    updated_at: string;
    items:Array<Item>;
    combos:Array<ComboItem>;
    constructor(menu_section:any){
        this.id = menu_section.id;
        this.name = menu_section.name;
        this.menu_id = menu_section.menu_id;
        this.created_at = menu_section.created_at;
        this.updated_at = menu_section.updated_at;
        this.items = [];

        for(let i in menu_section.items){
            let item = new Item(menu_section.items[i]);
            this.items.push(item);
        }

        this.combos = [];

        for(let i in menu_section.combos){
            let combo = new ComboItem(menu_section.combos[i]);
            this.combos.push(combo);
        }
    }

}

