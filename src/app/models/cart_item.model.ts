import { SlugifyPipe } from '../shared/slugify.pipe'
import { ItemVariation } from './item_variation.model'
import { Item } from './item.model'
export class CartItem {
    id: string;
    name: string;
    description: string;
    item: Item;
    selected_variation:ItemVariation;
    unit_price: number;
    subtotal_price: number;
    qty: number;
    type:string;
    formatted_extras:string;

    private slugifyPipe;


    initFromBasketItem(cart_item:CartItem){
        this.item = new Item(cart_item.item);
        this.name = cart_item.name;
        this.description = cart_item.description;
        this.slugifyPipe = new SlugifyPipe();
        this.qty = cart_item.qty;
        this.type = 'item';
        this.selected_variation = null;
        if(this.item.hasVariations()){
            let one_selected_variation = new ItemVariation(cart_item.selected_variation);
            console.log(one_selected_variation);
            this.selectVariation(one_selected_variation);
        }else{
            this.updateLocalId();
            this.updatePrices();
            this.updateFormattedExtras();
        }

    }






    initFromItem(item:Item){
        this.item = item;
        this.name = item.name;
        this.description = item.description;
        this.slugifyPipe = new SlugifyPipe();
        this.qty = 1;
        this.type = 'item';
        if(this.item.hasVariations()){
            this.selectVariation(this.item.getFirstVariation());
        }else{
            this.selected_variation = null;
            this.updateLocalId();
            this.updatePrices();
            this.updateFormattedExtras();
        }

    }

    getType(){
        return 'item';
    }

    getFormattedExtras(){
        let selected_variation_name = (this.selected_variation?this.selected_variation.name:'');
        return selected_variation_name;
    }


    decrement(num){
        this.qty = this.qty <=1 ? 1 : (this.qty - 1);
        this.updatePrices();
    }

    increment(num){
        this.qty = this.qty + num;
        this.updatePrices();
    }

    selectVariation(variation){
        this.selected_variation = variation;
        console.log('this.selected_variation');
        console.log(this.selected_variation);
        this.updateLocalId();
        this.updatePrices();
        this.updateFormattedExtras();
        console.log('word');
    }

    updateFormattedExtras(){
        this.formatted_extras = this.getFormattedExtras();
    }

    updatePrices(){
        this.updateUnitPrice();
        this.updateSubTotalPrice();
    }

    updateLocalId(){
        console.log('update local id');
        console.log(this.item.name);
        console.log(this);
        let selected_variation_name = (this.selected_variation?this.selected_variation.name:'0');
        this.id = this.slugifyPipe.transform(this.item.name+' '+selected_variation_name);
    }

    updateUnitPrice(){
        this.unit_price = this.getItemPrice()
    }

    updateSubTotalPrice(){
        this.subtotal_price = this.unit_price*this.qty;
    }

    getItemPrice(){
        if(this.item.variations.length == 0){
            return +this.item.base_price;
        }

        return +this.selected_variation.price;
    }

}

