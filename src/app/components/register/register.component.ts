import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user.model';
import { AuthService } from '../../services/auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {
  public loading;
  public error;
  public errors;
  public user:User;

  constructor(
    private router: Router, private auth:AuthService) { 
      this.loading = false;
      this.error = false;
      this.errors = [];
  }


  ngOnInit() {
    this.user = new User();
  }
  generateArray(obj){
    return Object.keys(obj).map((key)=>{ return {key:key, value:obj[key]}});
  }

  register(){
    this.loading = true;
    this.errors = [];
    this.error = false;
    this.auth.register(this.user).subscribe(
      data=>{
        this.loading = false;
        this.auth.logUserIn(data['success']);
        this.router.navigate(['']);
      },
      err => {
        this.loading = false;
        this.error = true;
        this.errors = err.error.error;
      },
      () => console.log('Done')
    );
  }

}
