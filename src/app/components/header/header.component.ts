import { Component, NgZone, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth/auth.service';
import { MenuService } from '../../services/menu/menu.service';
import { CartService } from '../../services/cart/cart.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public isLoggedIn;  
  constructor(private auth:AuthService, 
        private ngZone: NgZone,    

    private cartSelector:CartService,
    private menuSelector:MenuService, 
    private router:Router) {

    var _self = this;

    

    (<any>window).errorPaymentCallback = function(error){
      console.log(error);
      _self.auth.setGatewayError('We\'re sorry but something went wrong - try again later or another payment mode.');
    };
    
    (<any>window).cancelPaymentCallback = function(){
      console.log('wtf');
      _self.auth.setGatewayError( 'The transaction was cancelled');
    };

    (<any>window).successfulPaymentCallback = function(resultIndicator, sessionVersion){
      console.log(resultIndicator);
      console.log(sessionVersion);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  
      _self.menuSelector.markOrderCardPaid(_self.cartSelector.getOrder()).subscribe(
        data => { 
          _self.auth.setGatewayError('');
          _self.ngZone.run( () => {
              _self.router.navigate(['confirm']);
          });
        },
        err => console.error(err),
        () => console.log('done loading foods')
      );
          
    }


    this.isLoggedIn = this.auth.isLoggedIn();
  }

  ngOnInit() {
    this.isLoggedIn = this.auth.isLoggedIn();
  }

  logout(){
    this.auth.logout();
    this.router.navigate(['']);
  }

}
