import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { MenuService } from '../../services/menu/menu.service';
import { CartService } from '../../services/cart/cart.service';

import { NgxSmartModalService } from 'ngx-smart-modal';
import { CartItem } from '../../models/cart_item.model';
import { CartComboItem } from '../../models/cart_combo.model';
import { Basket } from '../../models/basket.model';
import { Menu } from '../../models/menu.model';
import { OrderService } from '../../services/order/order.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})

export class MenuComponent implements OnInit {

  // ng related constructors initializers

  private menus;
  private delivery_locations;
  private basket:Basket;
  private itemInFocus:CartItem;
  private comboInFocus;

  constructor(private menuSelector:MenuService,
    private router: Router, public ngxSmartModalService: NgxSmartModalService, private cartSelector:CartService) { }

  ngOnInit() {
    this.itemInFocus = null;
    this.comboInFocus = null;
    if(this.cartSelector.hasExistingCart()){
      this.basket = new Basket();
      this.basket.initFromBasket(this.cartSelector.getCart());
      console.log(this.basket);
    }else{
      this.basket = new Basket();
    }

    this.loadMenu();
    this.loadDeliveryLocations();
  }

  // menu related functions

  loadMenu(){
    try{
      let results:Array<any> = JSON.parse(localStorage.getItem(this.menuSelector.getKey()));
      this.menus =  results.map(function(menu_item){
        return new Menu(menu_item);
      });
    }catch(e){

      this.menuSelector.getRestaurantMenu().subscribe(
        data => { 
          this.menus = data;
          localStorage.setItem(this.menuSelector.getKey(), JSON.stringify(data));
          
        },
        err => console.error(err),
        () => console.log('done loading foods')
      );
    }
    
  }

  loadDeliveryLocations(){
    try{
      let results = JSON.parse(localStorage.getItem(this.menuSelector.getDeliveryLocationsKey()));
      this.delivery_locations = results;

    }catch(e){
      this.menuSelector.getDeliveryLocations().subscribe(
        data => { 
          this.delivery_locations = data;
          localStorage.setItem(this.menuSelector.getDeliveryLocationsKey(), JSON.stringify(data));
        },
        err => {console.error('err');console.error(err);},
        () => console.log('done loading')
      );

    }
   
  }

  // modal functions

  hasItemInFocus(){
    return this.itemInFocus == null ? false : true;
  }

  hasComboInFocus(){
    return this.comboInFocus == null ? false : true;
  }




  generateArray(obj){
    return Object.keys(obj).map((key)=>{ return {key:key, value:obj[key]}});
  }

  cartIsEmpty(){
    return (this.getCartTotalCount()) == 0 ? true : false;
  }

  getCartTotalCount(){
    return this.basket.count();
  }



  getItemableFromItem(item){
    let itemable = item;
    itemable['qty'] = 1;
    itemable['option'] = null;
    return itemable;
  }

  getComboableFromCombo(combo){
    let comboable = combo;
    comboable['qty'] = 1;
    comboable['options'] = {};
    return comboable;
  }
  
  openItemDetails(item){
    this.itemInFocus = new CartItem();
    this.itemInFocus.initFromItem(item);
    this.ngxSmartModalService.setModalData(this.itemInFocus, 'itemModal');
    this.ngxSmartModalService.getModal('itemModal').open();
  }

  toggleOptionCombo(comboInFocus, option_name, option){
    comboInFocus['options'][option_name] =  option;
  }
  
  closeItemDetails(){
    this.itemInFocus = null;
  }

  openComboDetails(combo){
    this.comboInFocus = new CartComboItem();
    this.comboInFocus.initFromCombo(combo);
    this.ngxSmartModalService.setModalData(this.comboInFocus, 'comboModal');
    this.ngxSmartModalService.getModal('comboModal').open();
  }

  closeComboDetails(){
    this.comboInFocus = null;
  }

  // cart related functions


  addItemToCart(item){
    this.basket.addItem(item);
    this.ngxSmartModalService.getModal('itemModal').close();    
  }

  proceedToCheckout(){
    this.cartSelector.saveCart(this.basket);
    this.router.navigate(['checkout']);
  }


  getCartItems(){
    return this.basket.items;
  }


  addComboToCart(combo){
    console.log(combo.validate());
    if(combo.validate()==false){
      return;
    }
    this.basket.addCombo(combo);
    this.ngxSmartModalService.getModal('comboModal').close();    
  }


}
