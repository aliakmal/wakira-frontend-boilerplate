import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { MenuService } from '../../services/menu/menu.service';
import { CartService } from '../../services/cart/cart.service';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent implements OnInit {
  private order;
  constructor(
    private router: Router, private menuSelector:MenuService, 
    private cartSelector:CartService) { 
    var _self = this;
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        _self.ngOnInit();
      }
    })
  }

  ngOnInit() {
    this.order = this.cartSelector.getOrder();
    console.log(this.order);
  }

}
